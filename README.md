ReadMe
==================================
Author: Riddhi Mankad 
Version: 1.0
Description: Fetch a contact from Salesforce

===================================

- API is HTTPS secure. The base URL is https://localhost:8082/api/
- The API specifications can be found here - https://localhost:8082/console/
- The documentation of the application is in docs/api.html and docs/fetchcontact.html files.  
- All the secrets are encrypted by the key using Secure Property Place holder. Ideally, the file containing the key should not be committed to the repository. However, I have committed for this application so that you can test the output.
- I have used basic Authentication to connect to Salesforce. We could use OAuth as well. 
- Best practice is to secure your API. You can apply different policies like Rate Limiting, IP filtering, Throttling, OAuth based authorization using API Manager once the application is deployed to CloudHub. However, for this test, the API is protected with API Key. You need to set Authorization header with this value 4a44daa1-e0f7-459d-aca1-e2ea669fa0fa

======================================
Testing the API
======================================

1. Use a tool like Postman 
2. Method Get https://localhost:8082/contacts/{emailaddress} 
3. Set Authorization header = 4a44daa1-e0f7-459d-aca1-e2ea669fa0fa


Questions?? 
Email: riddhi784@gmail.com